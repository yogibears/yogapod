<div class="event-info">
                        <div>
                        Power yoga is used to be described as a vigorous, vinyasa-style yoga. It originally closely resembled as ashtanga and was an attempt to make ashtanga more accessible to Western students. It differs, however, in that it is not a set series of poses, but rather allows the instructor freedom to teach what they want. Power Yoga is suitable for people who already have some experience with Yoga.</div>
</div>
