<script type="text/javascript">
<!--
var lang ="<?php echo $lang; ?>";

function redirect (link) {
location = link.href + "?lang=" + lang;
return false;
}
function resetit (lingo) {
lang=lingo;
return false;
}
// -->
</script>    
<meta name="Description" content="Yoga Pod brings the true benefits of Yoga to the local community in the most ideal practicing environment. Come and experience Yoga with us in a natural and blissful ecosystem at the heart of District 2, HCMC. Yoga Pod offers diversified classes which are suitable for all ages and levels along with flexible schedules throughout the week."/> 
<meta name="Keywords" content="Yoga, Yoga Studio, Yoga Pod, Studio, hcmc, Vietnam, Saigon, Thao Dien, fitness, wellness, lotus, crane, locust, posture, meditation, meditate, stretch, flexibility, relaxation, mind, body, spirit" />
<meta name="author" content="Yoga Pod">
<meta name="robots" content="index, follow">
<meta name="revisit-after" content="1 days">
<meta name="referrer" content="always" />
<link rel="canonical" href="http://www.yogapodsaigon.com"/>
<meta name="geo.region" content="VN-SG" />
<meta name="geo.placename" content="Hồ Ch&iacute; Minh" />
<meta name="geo.position" content="10.809036;106.732448" />
<meta name="ICBM" content="10.809036, 106.732448" />

</head>
<body>
<div class="page">
    <!--========================================================
                              HEADER
    =========================================================-->
    <header>
        <div id="stuck_container" class="stuck_container">
            <div class="container">
                <div class="brand">
                    <div class="brand_img">
                        <img src="images/lago.png" width="65px" height="65px" alt=""/>
                    </div>
                    <h1 class="brand_name">
                        <a href="index.php" onclick="javascript:redirect(this)">YOGA POD</a>
                    </h1>
                </div>

                <nav class="nav">
                    <ul class="sf-menu" data-type="navbar">
                        <li <?php echo $home ?>>
                            <a class="vietnam" style="display: none;" href="index.php" onclick="return redirect(this)">NHÀ</a>  
                            <a class="english" style="display: none;" href="index.php" onclick="return redirect(this)">HOME</a>
                        </li>
						
                        <li <?php echo $classes ?>> 
				            <a class="vietnam" style="display: none;" href="classes.php" onclick="return redirect(this)">CÁC LỚP HỌC</a>
                            <a class="english" style="display: none;" href="classes.php" onclick="return redirect(this)">Classes</a>
                        </li>
<!--	REMOVE FOR NOW						
			<li <?php echo $pricing ?>>
				<a class="vietnam" style="display: none;" href="#">GIÁ</a>
                <a class="english" style="display: none;" href="#">Pricing</a>
				<ul>
					<li>
						<a class="vietnam" style="display: none;" href="passes.php" onclick="return redirect(this)">Đ PC ĐIỂM THAM QUAN </a>
                        <a class="english" style="display: none;" href="passes.php" onclick="return redirect(this)">Pass Packages </a>
					</li>

					<li>
						<a href="#">Fermentum nisl</a>
						<ul>
						<li>
							<a href="#">Consequat ante</a>
						</li>
						<li>
							<a href="#">Lorem ipsum dol</a>
						</li>
						<li>
							<a href="#">Sit amet</a>
						</li>
						<li>
							<a href="#">Consectetuer</a>
						</li>
						</ul>
					</li>
					
					<li>
					<a class="vietnam" style="display: none;" href="memberships.php" onclick="return redirect(this)">THÀNH VIÊN  </a>
                    <a class="english" style="display: none;" href="memberships.php" onclick="return redirect(this)">Memberships </a>
					</li>
				</ul>
			</li>
-->			
                        <li <?php echo $gallery ?>>
                            <a class="vietnam" style="display: none;" href="gallery.php" onclick="return redirect(this)">BỘ SƯU TẬP</a>
                            <a class="english" style="display: none;" href="gallery.php" onclick="return redirect(this)">Gallery</a>
                        </li>
                        <li <?php echo $vr ?>>
                            <a class="vietnam" style="display: none;" href="vr.php" onclick="return redirect(this)">360</a>
                            <a class="english" style="display: none;" href="vr.php" onclick="return redirect(this)">360</a>
                        </li>
                        <li <?php echo $yogis ?>>
                            <a class="vietnam" style="display: none;" href="yogis.php" onclick="return redirect(this)">Giảng viên</a>
                            <a class="english" style="display: none;" href="yogis.php" onclick="return redirect(this)">Instructors</a>
                        </li>						
                        <li <?php echo $contact ?>>
                            <a class="vietnam" style="display: none;" href="contact.php" onclick="return redirect(this)">LIÊN HỆ</a>
                            <a class="english" style="display: none;" href="contact.php" onclick="return redirect(this)">Contact </a>
                        </li>
                        <li>
                            <a href="#" onClick='$(".english").hide(); $(".vietnam").show();<?php $lang ="vietnam"; ?> lang="vietnam"; return resetit("vietnam");' id="viet"><img src="images/vn.png" width="22px" height="145px" alt=""/></a>
                        </li>
                        <li>
                            <a href="#" onClick='$(".vietnam").hide(); $(".english").show();<?php $lang ="english"; ?> lang="english";  return resetit("english");' id="english"><img src="images/gb.png" width="22px" height="145px" alt=""/></a>
                        </li>                        
                    </ul>
                </nav>
            </div>
      </div>
         <script>
         $(document).ready(function(){
           if (lang == "vietnam" ) {
                $(".english").hide();
                $(".vietnam").show();
            } else {
                $(".vietnam").hide();
                $(".english").show();
            }      
         });
         $(document).ready(function(){
            $( "#viet" ).click(function() {
                $(".english").hide();
                $(".vietnam").show();
                <?php $lang ="vietnam"; ?>
                lang="vietnam";
            });
            $( "#english" ).click(function() {
                $(".vietnam").hide();
                $(".english").show();
                <?php $lang ="english"; ?>
                lang="english";
            });
        });
  
</script>  
</header>

