<?php
$home = '';
$about = '';
$classes = '';
$news = '';
$vr = 'class="active"';
$contact = '';
$gallery = '';
?>
<html class="no-js" lang="en">
<head>

    <title>Yoga Pod - 360 view</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="css/vr.css" />

<?php
$lang = $_GET['lang'];
if ($lang == "" or $lang == "english") {
    $lang = "english";
} else {
    $lang = "vietnam";
}
?>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no"/>
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/camera.css"/>
    <link rel="stylesheet" href="css/owl-carousel.css"/>
	

	
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>
    <!--[if lt IE 9]>
    <html class="lt-ie9">
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/..">
            <img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"
                 alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
        </a>
    </div>
    <script src="js/html5shiv.js"></script>
    <![endif]-->

<script src='js/device.min.js'></script>

<?php include 'Header.php';?>

    <!--========================================================
                              CONTENT
    =========================================================-->
    <main>
		
		
		<section class="well well__ins1 parallax parallax4" data-url="images/parallax4.jpg" data-mobile="false" data-speed="0.5">
            <div class="container">
                <h4 class="center">Studio 360 Images</h4>

                <div class="row">
                    <div class="grid_6 wow fadeInLeft" style="border-style: solid;border-color: #ffffff;z-index:100">
						<div id="your-pano"></div>
                    </div>
                    
					<div class="grid_6 wow fadeInRight" id="360_thumbs">
                        <div class="box box_add wow fadeInRight" >
                            <div class="box_aside"><img id="360_img_1" src="images/page-2_img02.jpg" alt=""/>
                            </div>
                            <div class="box_cnt box_cnt__no-flow">
                                <p style="color:#ffffff;"> Studio 2 Opens to a beautiful deck, bringing your experince a step closer to nature</p>
                            </div>
                        </div>
                        <div class="box  wow fadeInRight" data-wow-delay="0.2s">
                            <div class="box_aside"><img id="360_img_2" src="images/page-2_img03.jpg" alt=""/>
                            </div>
                            <div class="box_cnt box_cnt__no-flow">
                                <p style="color:#ffffff;">Soft Opening Day</p>
                            </div>
                        </div>
                        <div class="box     wow fadeInRight " data-wow-delay="0.4s">
                            <div class="box_aside"><img id="360_img_3" src="images/page-2_img04.jpg" alt=""/>
                            </div>
                            <div class="box_cnt box_cnt__no-flow">
                                <p style="color:#ffffff;">Open door Yoga Class</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		
    </main>
<?php include 'Footer.php';?>

</div>
<!-- External library -->
<script src="js/three.min.js"></script>

<!-- External library, but included in the build -->
<script src="js/sphoords.js"></script>

<!-- Photo Sphere Viewer files -->
<script src="js/PhotoSphereViewer.js"></script>
<script src="js/PSVNavBar.js"></script>
<script src="js/PSVNavBarButton.js"></script>
<script src="js/script.js"></script>

<script type="text/javascript">




$(document).ready(function() {
	
    $.fn.myFunction = function(param) { 

			PSV = new PhotoSphereViewer({
			// Panorama, given in base 64
			panorama: param,

			// Container
			container: 'your-pano',

			// Deactivate the animation
			time_anim: false,

			// Display the navigation bar
			navbar: true,

			// Resize the panorama
			size: {
				width: '100%',
				height: '500px'
			},

			// No XMP data
			usexmpdata: false

	});	
	};
	
	$.fn.myFunction("vr/360_0510_Stitch_XHC_comp2.jpg"); 


	
	$( "#360_img_1" ).click(function() {
		$.fn.myFunction("vr/360_0510_Stitch_XHC_comp2.jpg"); 
	});
	$( "#360_img_2" ).click(function() {
		$.fn.myFunction("vr/360_0430_Stitch_XHC_comp2.jpg");
	});
	$( "#360_img_3" ).click(function() {
		$.fn.myFunction("vr/360_0493_Stitch_XHC_comp2.jpg"); 
	});
	
});


	
</script>		

</body>
</html>