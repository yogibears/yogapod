<div class="event-info">
                        <div>*****
                        Flow Yoga is a relatively challenging class with a combination of energy-intensive postures that go hand-in-hand with relaxation exercises. Created to challenge physical and mental strength, the class improves health and endurance, while enhancing cardiovascular health. Suitable for practicing people at all levels.</div>
</div>
