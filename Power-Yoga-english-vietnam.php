<div class="event-info">
                        <div>
                        Power yoga được hiểu như là một thể loại Vinyasa yoga hết sức mạnh mẽ. Power yoga có nguồn gốc từ Ashtanga yoga được điều chỉnh để dễ dàng hoà hợp với các học viên phương Tây. Tuy nhiên, Power yoga khách ở chỗ nó không phải là một chuỗi động tác cố định, mà là một chuỗi tự do theo sự sáng tạo của giáo viên. Lớp học này phù hợp với những người đã có kinh nghiệm luyên tập.</div>                    
</div>
