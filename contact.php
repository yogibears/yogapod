<?php
$contact = 'class="active"';
?>
<html lang="en">
<head>
    <link rel="stylesheet" href="css/mailform.css"/>
    <link rel="stylesheet" href="css/google-map.css"/>
    <title>Yoga Pod - Contact</title>

	
<?php
$lang = $_GET['lang'];
if ($lang == "" or $lang == "english") {
    $lang = "english";
} else {
    $lang = "vietnam";
}
?>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no"/>
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/camera.css"/>
    <link rel="stylesheet" href="css/owl-carousel.css"/>
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script>

    <!--[if lt IE 9]>
    <html class="lt-ie9">
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/..">
            <img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"
                 alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
        </a>
    </div>
    <script src="js/html5shiv.js"></script>
    <![endif]-->

    <script src='js/device.min.js'></script>

<?php include 'Header.php';?>	
	
<!-- Google Code for Navigate to Contact Us Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 845905773;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "dQexCPK1_3IQ7f6tkwM";
var google_conversion_value = 150000.00;
var google_conversion_currency = "VND";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/845905773/?value=150000.00&amp;currency_code=VND&amp;label=dQexCPK1_3IQ7f6tkwM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
	
    <!--========================================================
                              CONTENT
    =========================================================-->
    <main>
        <section class="well well__ins5">
            <div class="container">
                <div class="row">
                    <div class="grid_3">
                        <h4 class="secondary-color center">address</h4>
                        <address>28 Đường Thảo Điền, Thảo Điền, Quận 2,<BR>Hồ Chí Minh, Vietnam.
                        </address>
                        <ul class="contact-list">
                           <li>
                                <dl>
                                    <dt>m:&nbsp;<a href="callto:x">+84 (0) 128 907 6997</a></dt>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>e:&nbsp;<a href="mailto:x">info@yogapodsaigon.com</a></dt>
                                </dl>
                            </li>
							<li>
                                <dl>
                                    <dt>&nbsp;</dt>
                                </dl>
                            </li>							
							<li>
                                <dl>
                                    <dt><a target="_blank" title="find us on Facebook" href="http://www.facebook.com/yogapodsaigon"><img alt="follow me on facebook" src="//login.create.net/images/icons/user/facebook_30x30.png" border=0></a>&nbsp;&nbsp;&nbsp;<a target="_blank" title="follow me on instagram" href="http://www.instagram.com/yogapodsaigon"><img alt="follow me on instagram" src="https://c866088.ssl.cf3.rackcdn.com/assets/instagram30x30.png" border=0></a>&nbsp;&nbsp;&nbsp;<a target="_blank" title="follow me on youtube" href="http://www.youtube.com/channel/UCvFsfyCpSE_pcoyHWq8GmTg"><img alt="follow me on youtube" src="https://c866088.ssl.cf3.rackcdn.com/assets/youtube30x30.png" border=0></a>&nbsp;&nbsp;&nbsp;<a target="_blank" title="follow me on twitter" href="http://www.twitter.com/yogapodsaigon"><img alt="follow me on twitter" src="//login.create.net/images/icons/user/twitter_30x30.png" border=0></a></dt>
                                </dl>
                            </li>

                        </ul>

                    </div>
                    <div class="grid_9">
                        <h4 class="secondary-color center">CONTACT FORM</h4>

                        <div class="row">
                            <form class='mailform' method="post" action="bat/rd-mailform.php">
                                <input type="hidden" name="form-type" value="contact"/>
                                <fieldset>
                                    <div class="grid_3">
                                        <label data-add-placeholder>
                                            <input type="text"
                                                   name="name"
                                                   placeholder="Name*"
                                                   data-constraints="@LettersOnly @NotEmpty"/>
                                        </label>
                                    </div>
                                    <div class="grid_3">
                                        <label data-add-placeholder>
                                            <input type="text"
                                                   name="email"
                                                   placeholder="E-mail*"
                                                   data-constraints="@Email @NotEmpty"/>
                                        </label>
                                    </div>
                                    <div class="grid_3">
                                        <label data-add-placeholder>
                                            <input type="text"
                                                   name="phone"
                                                   placeholder="Phone"
                                                   data-constraints="@Phone"/>
                                        </label>
                                    </div>
                                    <div class="grid_9">
                                        <label data-add-placeholder>
                                    <textarea name="message" placeholder="Message*"
                                              data-constraints="@NotEmpty"></textarea>
                                        </label>

                                        <div class="mfControls">
                                            <button class="btn btn_mod1" type="submit">send message
                                            </button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="map">
                <div id="google-map" class="map_model" ></div>
                <ul class="map_locations">
                            <li data-x="106.738356" data-y="10.806172">
					            <p> 28 Thao Dien, Quan 2,<BR>Ho Chi Minh, Vietnam
					                <span>+84 (0) 128 907 6997</span>
					            </p>
        </li>
                </ul>
            </div>
        </section>

    </main>
<?php include 'Footer.php';?>
</div>

<script src="js/script.js"></script>
</body>
</html>