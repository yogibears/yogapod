<?php
$about = 'class="active"';
?>
<html lang="en">
<head>
    <title>Yoga Pod - About</title>
<?php include 'Header.php';?>
    <!--========================================================
                              CONTENT
    =========================================================-->
    <main>
        <section class="well well__ins4">
            <div class="container center">
                <div class="row">
                    <div class="preffix_1 grid_10">
                        <h4 class="secondary-color">ABOUT ME</h4>
                        <img class="br-radius" src="images/page-3_img01.jpg" alt=""/>

                        <p class="primary-color">Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius
                            mi. Cum sociis natoque penatibus et
                            magnis dis parturient montes, nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada
                            odio.</p>
                        <p>Morbi nunc odio, gravida at, cursus nec, luctus a, lorem. Maecenas tristique orci ac sem.
                            Duis ultricies pharetra magna. Donec accumsan malesuada orci. Donec sit amet eros. Lorem
                            ipsum dolor sit amet, consectetuer adipiscing elit. Mauris fermentum dictum magna. Sed
                            laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit.
                            Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu
                            lacus. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum
                            molestie lacus. Aenean nonummy hendrerit mauris. </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="well2 bg-secondary2">
            <div class="container">
                <div class="row">
                    <div class="preffix_6 grid_6">
                        <h2>Never stopping
                            at the achieved </h2>

                        <p>Elementum velitIpede mi, aliquet sit ametuismodIn pede mi, ali quet sit amet, euismod
                            in,auctor ut, ligulaLorem ipsum dolor sit amet, consectetuer adipiscing eliaesent
                            vestibulum.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="well well__ins4 parallax parallax5" data-url="images/parallax5.jpg" data-mobile="false"
                 data-speed="0.5">
            <div class="container center">
                <h4>MY HAPPY CLIENTS</h4>
                <h6>Nullam sed velit quam. Pellentesque at neque vulpu-habitant morbi tristique senectus
                    et netus et malesuada fames ac turpis egestas.</h6>

                <div class="row">
                    <div class="grid_3 wow fadeInLeft" data-wow-delay="0.2s">
                        <div class="img_wrapp"><img src="images/page-3_img02.jpg" alt=""/></div>
                        <h6 class="primary-color">Caroline Beek</h6>

                        <p>Consectetuer adipiscing elit.
                            Praesent vestibulum</p>
                    </div>
                    <div class="grid_3 wow fadeInLeft" >
                        <div class="img_wrapp"><img src="images/page-3_img03.jpg" alt=""/></div>
                        <h6 class="primary-color">Gloria Mann</h6>

                        <p>
                            Ametuis modIn pede, aliquet
                            amet, euismod in,auctor</p>
                    </div>
                    <div class="grid_3 wow fadeInRight">
                        <div class="img_wrapp"><img src="images/page-3_img04.jpg" alt=""/></div>
                        <h6 class="primary-color">Patrick Pool</h6>

                        <p>Ametuis modIn pede, aliquet
                            amet, euismod in,auctor</p>
                    </div>
                    <div class="grid_3 wow fadeInRight" data-wow-delay="0.2s">
                        <div class="img_wrapp"><img src="images/page-3_img05.jpg" alt=""/></div>
                        <h6 class="primary-color">Alan Smith</h6>

                        <p>Ametuis modIn pede, aliquet
                            amet, euismod in,auctor</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="well well__ins4">
            <div class="container">
                <h4 class="center secondary-color">My Philosophy</h4>
                <h6 class="center">Elementum velitIpede mi, aliquet sit ametuismodIn pede mi, aliquet sit amet, euismod
                    in,auctor ut,
                    ligulaLorem ipsum dolor sit amet, consectetuer adipiscing elit. </h6>

                <ul class="row index-list">
                    <li class="grid_3 wow fadeInLeft" data-equal-group="3" data-wow-delay="0.6s">
                        <div class="wrapp">
                            <h6>Fusce suscipit varius mi </h6>

                            <p>Praesent vestibulum molest
                                ie lacus. Aenean nonummy erit mauris. Phasellus porta. Fusce suscipit varius mi. Cum
                                sociis
                                natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
                        </div>
                    </li>
                    <li class="grid_3 wow fadeInLeft" data-equal-group="3" data-wow-delay="0.4s">
                        <div class="wrapp">
                            <h6>Praesent vestibulum mol</h6>

                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elitris fermentum dictum magna. Sed
                                laoreet aliquam leo. Ut tellus dolor, dapibus eget, elemetum vel, cursus eleifend,
                                elitenean
                                auctor wisi et urna. </p>
                        </div>
                    </li>
                    <li class="grid_3 wow fadeInLeft" data-equal-group="3" data-wow-delay="0.2s">
                        <div class="wrapp">
                            <h6>Donec accumsan males</h6>

                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elitris fermentum dictum magna. Sed
                                laoreet aliquam leo. Ut tellus dolor, dapibus eget, eleme tum vel, cursus eleifend, elit
                                enean auctor wisi et urna uame.</p>
                        </div>
                    </li>
                    <li class="grid_3 wow fadeInLeft" data-equal-group="3">
                        <div class="wrapp">
                            <h6>Lorem ipsum dolor sit</h6>

                            <p>Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum
                                sociis
                                natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                                Fusce
                                feugiat malesuada odio. </p>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <section class="well well__ins4 parallax parallax6" data-url="images/parallax6.jpg" data-mobile="false"
                 data-speed="0.5">
            <div class="container">
                <h4 class="center">Choose Your Goal</h4>
                <h6 class="center">Duis ultricies pharetra magna. Donec accumsan malesuada orci. Donec sit amet eros.
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </h6>

                <p class="center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie
                    lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis
                    natoque penatibus et magnis dis parturient montes,
                    nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada odio. Morbi nunc odio, gravida at,</p>

                <div class="row">
                    <div class="preffix_2 grid_4">
                        <ul>
                            <li class="primary-color wow fadeInLeft"><a href="#">- sociis natoque penatibus et magnis </a></li>
                            <li class="primary-color wow fadeInLeft" data-wow-delay="0.2s"><a href="#">- accumsan malesuada orci</a></li>
                            <li class="primary-color wow fadeInLeft" data-wow-delay="0.4s"><a href="#">- ipsum dolor sit amet consectetuer adipiscing </a>
                            </li>
                            <li class="primary-color wow fadeInLeft" data-wow-delay="0.6s"><a href="#">- vestibulum molestie lacus enean nonummy</a></li>
                        </ul>
                    </div>
                    <div class="grid_5">
                        <ul>
                            <li class="primary-color wow fadeInRight" ><a href="#">- penatibus et magnis dis parturient</a></li>
                            <li class="primary-color wow fadeInRight" data-wow-delay="0.2s"><a href="#">- consectetuer adipiscing ultricies pharetra</a></li>
                            <li class="primary-color wow fadeInRight" data-wow-delay="0.4s"><a href="#">- dapibus eget elementum vel cursus</a></li>
                            <li class="primary-color wow fadeInRight" data-wow-delay="0.6s"><a href="#">- penatibus et magnis ultricies phare parturient</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </main>

<?php include 'Footer.php';?>

</div>

<script src="js/script.js"></script>
</body>
</html>