<?php
$news = 'class="active"';
?>
<html class="no-js" lang="en">
<head>

    <title>Yoga Pod - News</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="css/vr.css" />

<?php
$lang = $_GET['lang'];
if ($lang == "" or $lang == "english") {
    $lang = "english";
} else {
    $lang = "vietnam";
}
?>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no"/>
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/camera.css"/>
    <link rel="stylesheet" href="css/owl-carousel.css"/>
	

<!-- Demo CSS -->
<link rel="stylesheet" href="css/demo.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />

<!-- Modernizr -->
<script src="js/modernizr.js"></script>	
	
	
	
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>
    <!--[if lt IE 9]>
    <html class="lt-ie9">
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/..">
            <img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"
                 alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
        </a>
    </div>
    <script src="js/html5shiv.js"></script>
    <![endif]-->

<script src='js/device.min.js'></script>
<script type="text/javascript">
<!--
var lang ="<?php echo $lang; ?>";

function redirect (link) {
location = link.href + "?lang=" + lang;
return false;
}
function resetit (lingo) {
lang=lingo;
return false;
}
// -->
</script>    



      
</head>
<body>
<div class="page">
    <!--========================================================
                              HEADER
    =========================================================-->
    <header>
        <div id="stuck_container" class="stuck_container">
            <div class="container">
                <div class="brand">
                    <div class="brand_img">
                        <img src="images/lago.png" width="65px" height="65px" alt=""/>
                    </div>
                    <h1 class="brand_name">
                        <a href="index.php" onclick="javascript:redirect(this)">YOGA POD</a>
                    </h1>
                </div>

                <nav class="nav">
                    <ul class="sf-menu" data-type="navbar">
                        <li <?php echo $home ?>>
                            <a class="vietnam" style="display: none;" href="index.php" onclick="return redirect(this)">NHÀ</a>  
                            <a class="english" style="display: none;" href="index.php" onclick="return redirect(this)">HOME</a>
                        </li>
                        <li <?php echo $news ?>>
                            <a class="vietnam" style="display: none;" href="news.php" onclick="return redirect(this)">TIN TỨC</a>
                            <a class="english" style="display: none;" href="news.php" onclick="return redirect(this)">News</a>
                        </li>
                        <li <?php echo $contact ?>>
                            <a class="vietnam" style="display: none;" href="contact.php" onclick="return redirect(this)">TIẾP XÚC</a>
                            <a class="english" style="display: none;" href="contact.php" onclick="return redirect(this)">Contact </a>
                        </li>
                        <li>
                            <a href="#" onClick='$(".english").hide(); $(".vietnam").show();<?php $lang ="vietnam"; ?> lang="vietnam"; return resetit("vietnam");' id="viet"><img src="images/vn.png" width="22px" height="145px" alt=""/></a>
                        </li>
                        <li>
                            <a href="#" onClick='$(".vietnam").hide(); $(".english").show();<?php $lang ="english"; ?> lang="english";  return resetit("english");' id="english"><img src="images/gb.png" width="22px" height="145px" alt=""/></a>
                        </li>                        
                    </ul>
                </nav>
            </div>
      </div>
         <script>
         $(document).ready(function(){
           if (lang == "vietnam" ) {
                $(".english").hide();
                $(".vietnam").show();
            } else {
                $(".vietnam").hide();
                $(".english").show();
            }      
         });
         $(document).ready(function(){
            $( "#viet" ).click(function() {
                $(".english").hide();
                $(".vietnam").show();
                <?php $lang ="vietnam"; ?>
                lang="vietnam";
            });
            $( "#english" ).click(function() {
                $(".vietnam").hide();
                $(".english").show();
                <?php $lang ="english"; ?>
                lang="english";
            });
        });
  
</script>  
</header>
    <!--========================================================
                              CONTENT
    =========================================================-->
    <main>


		
<section class="well well__ins4">
<div id="container" class="cf">
    <header role="navigation"></header>
			
		<div id="main" role="main">
      <section class="slider">
        <div id="slider" class="flexslider">
          <ul class="slides">
<li><img src="gallery/IMG_0511.jpg" /></li>
<li><img src="gallery/IMG_0535.jpg" /></li>
<li><img src="gallery/IMG_0541.jpg" /></li>
<li><img src="gallery/IMG_0552.jpg" /></li>
<li><img src="gallery/IMG_0554.jpg" /></li>
<li><img src="gallery/IMG_0558.jpg" /></li>
<li><img src="gallery/IMG_0563.jpg" /></li>
<li><img src="gallery/IMG_0569.jpg" /></li>
<li><img src="gallery/IMG_0574.jpg" /></li>
<li><img src="gallery/IMG_0577.jpg" /></li>
<li><img src="gallery/IMG_0579.jpg" /></li>
<li><img src="gallery/IMG_0587.jpg" /></li>
<li><img src="gallery/IMG_0590.jpg" /></li>
<li><img src="gallery/IMG_0595.jpg" /></li>
<li><img src="gallery/IMG_0613.jpg" /></li>
<li><img src="gallery/IMG_0634.jpg" /></li>
<li><img src="gallery/IMG_0658.jpg" /></li>
<li><img src="gallery/IMG_0661.jpg" /></li>
<li><img src="gallery/IMG_0667.jpg" /></li>
<li><img src="gallery/IMG_0688.jpg" /></li>
<li><img src="gallery/IMG_0694.jpg" /></li>
<li><img src="gallery/TTT_2402.jpg" /></li>
<li><img src="gallery/TTT_2415.jpg" /></li>
<li><img src="gallery/TTT_2416.jpg" /></li>
<li><img src="gallery/TTT_2421.jpg" /></li>
<li><img src="gallery/TTT_2422.jpg" /></li>
<li><img src="gallery/TTT_2431.jpg" /></li>
<li><img src="gallery/TTT_2445.jpg" /></li>
<li><img src="gallery/TTT_2472.jpg" /></li>
<li><img src="gallery/TTT_2488.jpg" /></li>
<li><img src="gallery/TTT_2489.jpg" /></li>
<li><img src="gallery/TTT_2492.jpg" /></li>
<li><img src="gallery/TTT_2508.jpg" /></li>
<li><img src="gallery/TTT_2511.jpg" /></li>
<li><img src="gallery/TTT_2538.jpg" /></li>
<li><img src="gallery/TTT_2539.jpg" /></li>
<li><img src="gallery/TTT_2542.jpg" /></li>
<li><img src="gallery/TTT_2544.jpg" /></li>
<li><img src="gallery/TTT_2547.jpg" /></li>
<li><img src="gallery/TTT_2553.jpg" /></li>
<li><img src="gallery/TTT_2558.jpg" /></li>
<li><img src="gallery/TTT_2562.jpg" /></li>
<li><img src="gallery/TTT_2564.jpg" /></li>
<li><img src="gallery/TTT_2580.jpg" /></li>
<li><img src="gallery/TTT_2584.jpg" /></li>
<li><img src="gallery/TTT_2585.jpg" /></li>
<li><img src="gallery/TTT_2586.jpg" /></li>
<li><img src="gallery/TTT_2602.jpg" /></li>
<li><img src="gallery/TTT_2609.jpg" /></li>
<li><img src="gallery/TTT_2616.jpg" /></li>
<li><img src="gallery/TTT_2622.jpg" /></li>
<li><img src="gallery/TTT_2626.jpg" /></li>
<li><img src="gallery/TTT_2629.jpg" /></li>
<li><img src="gallery/TTT_2630.jpg" /></li>
<li><img src="gallery/TTT_2634.jpg" /></li>
<li><img src="gallery/TTT_2636.jpg" /></li>
<li><img src="gallery/TTT_2637.jpg" /></li>
<li><img src="gallery/TTT_2644.jpg" /></li>
<li><img src="gallery/TTT_2646.jpg" /></li>

          </ul>
        </div>
        <div id="carousel" class="flexslider">
          <ul class="slides">
<li><img src="gallery/IMG_0511.jpg" /></li>
<li><img src="gallery/IMG_0535.jpg" /></li>
<li><img src="gallery/IMG_0541.jpg" /></li>
<li><img src="gallery/IMG_0552.jpg" /></li>
<li><img src="gallery/IMG_0554.jpg" /></li>
<li><img src="gallery/IMG_0558.jpg" /></li>
<li><img src="gallery/IMG_0563.jpg" /></li>
<li><img src="gallery/IMG_0569.jpg" /></li>
<li><img src="gallery/IMG_0574.jpg" /></li>
<li><img src="gallery/IMG_0577.jpg" /></li>
<li><img src="gallery/IMG_0579.jpg" /></li>
<li><img src="gallery/IMG_0587.jpg" /></li>
<li><img src="gallery/IMG_0590.jpg" /></li>
<li><img src="gallery/IMG_0595.jpg" /></li>
<li><img src="gallery/IMG_0613.jpg" /></li>
<li><img src="gallery/IMG_0634.jpg" /></li>
<li><img src="gallery/IMG_0658.jpg" /></li>
<li><img src="gallery/IMG_0661.jpg" /></li>
<li><img src="gallery/IMG_0667.jpg" /></li>
<li><img src="gallery/IMG_0688.jpg" /></li>
<li><img src="gallery/IMG_0694.jpg" /></li>
<li><img src="gallery/TTT_2402.jpg" /></li>
<li><img src="gallery/TTT_2415.jpg" /></li>
<li><img src="gallery/TTT_2416.jpg" /></li>
<li><img src="gallery/TTT_2421.jpg" /></li>
<li><img src="gallery/TTT_2422.jpg" /></li>
<li><img src="gallery/TTT_2431.jpg" /></li>
<li><img src="gallery/TTT_2445.jpg" /></li>
<li><img src="gallery/TTT_2472.jpg" /></li>
<li><img src="gallery/TTT_2488.jpg" /></li>
<li><img src="gallery/TTT_2489.jpg" /></li>
<li><img src="gallery/TTT_2492.jpg" /></li>
<li><img src="gallery/TTT_2508.jpg" /></li>
<li><img src="gallery/TTT_2511.jpg" /></li>
<li><img src="gallery/TTT_2538.jpg" /></li>
<li><img src="gallery/TTT_2539.jpg" /></li>
<li><img src="gallery/TTT_2542.jpg" /></li>
<li><img src="gallery/TTT_2544.jpg" /></li>
<li><img src="gallery/TTT_2547.jpg" /></li>
<li><img src="gallery/TTT_2553.jpg" /></li>
<li><img src="gallery/TTT_2558.jpg" /></li>
<li><img src="gallery/TTT_2562.jpg" /></li>
<li><img src="gallery/TTT_2564.jpg" /></li>
<li><img src="gallery/TTT_2580.jpg" /></li>
<li><img src="gallery/TTT_2584.jpg" /></li>
<li><img src="gallery/TTT_2585.jpg" /></li>
<li><img src="gallery/TTT_2586.jpg" /></li>
<li><img src="gallery/TTT_2602.jpg" /></li>
<li><img src="gallery/TTT_2609.jpg" /></li>
<li><img src="gallery/TTT_2616.jpg" /></li>
<li><img src="gallery/TTT_2622.jpg" /></li>
<li><img src="gallery/TTT_2626.jpg" /></li>
<li><img src="gallery/TTT_2629.jpg" /></li>
<li><img src="gallery/TTT_2630.jpg" /></li>
<li><img src="gallery/TTT_2634.jpg" /></li>
<li><img src="gallery/TTT_2636.jpg" /></li>
<li><img src="gallery/TTT_2637.jpg" /></li>
<li><img src="gallery/TTT_2644.jpg" /></li>
<li><img src="gallery/TTT_2646.jpg" /></li>
          </ul>
        </div>
      </section>

    </div>		
			
			
</div>
</section>		
		
		
    </main>
<?php include 'Footer.php';?>

</div>
		

  <!-- jQuery -->
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script>
  <!-- FlexSlider -->
  <script defer src="js/jquery.flexslider.js"></script>

  <script type="text/javascript">
    $(function(){
      SyntaxHighlighter.all();
    });
    $(window).load(function(){
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 210,
        itemMargin: 5,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
  </script>


  <!-- Syntax Highlighter -->
  <script type="text/javascript" src="js/shCore.js"></script>
  <script type="text/javascript" src="js/shBrushXml.js"></script>
  <script type="text/javascript" src="js/shBrushJScript.js"></script>

  <!-- Optional FlexSlider Additions -->
  <script src="js/jquery.easing.js"></script>
  <script src="js/jquery.mousewheel.js"></script>
  <script defer src="js/demo.js"></script>
</body>
</html>