<div class="event-info">
                        <div>
                        Vinyasa là thể loại yoga kết nối giữa chuyển động và hơi thở, tạo thành chuỗi các động tác chuyển tiếp nhẹ nhàng. Theo tiếng Ấn Độ, “vinyasa” có nghĩa là “kết nối”. Từng chuyển động được kết hợp nhịp nhàng với từng nhịp hít vào, thở ra. Mỗi buổi tập “ Vinyasa” sẽ kết thúc bằng tư thế nghỉ ngơi.<BR><BR>Không có khuôn phép ngặt nghèo hay chuỗi động tác nào đặc biệt trong lớp tập Vinyasa.<BR><BR>Mỗi chuỗi yoga được hình thành dựa trên nhu cầu cũng như sức sáng tạo của giáo viên khi kết hợp các thế yoga một cách nhuần nhuyễn và logic. Tuỳ thuộc vào từng cấp độ, lớp vinyasa yoga có thể ở mức độ nhẹ nhàng hoặc mạnh mẽ.</div>                        
</div>
