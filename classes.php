<?php
$home = '';
$about = '';
$classes = 'class="active"';
$news = '';
$contact = '';
$gallery = '';
?>
<!doctype html>   
<html lang="en">
<head>
    <title>Yoga Pod - Classes</title>

<?php
$lang = $_GET['lang'];
if ($lang == "" or $lang == "english") {
    $lang = "english";
} else {
    $lang = "vietnam";
}
?>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no"/>
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/camera.css"/>
    <link rel="stylesheet" href="css/owl-carousel.css"/>
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script>

    <!--[if lt IE 9]>
    <html class="lt-ie9">
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/..">
            <img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"
                 alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
        </a>
    </div>
    <script src="js/html5shiv.js"></script>
    <![endif]-->

    <script src='js/device.min.js'></script>
	
<?php include 'Header.php';?>
<!--========================================================
                              CONTENT
    =========================================================-->
    <main>
        <section class="well well__ins3">
            <div class="container">
                <h4 class="english center secondary-color" style="display:none" >Start your practice efficiently with impactful habits</h4>
                <h4 class="vietnam center secondary-color" style="display:none" >Bắt đầu luyện tập với những thói quen tốt</h4>

                <div class="row offset">
                    <div class="grid_5">
                        <div class="center "><img class="br-radius img-add" src="images/page-2_img01.jpg" alt=""/></div>
                    </div>
                    <div class="grid_7">
                        <h6 class="wow fadeInRight">Too many types of classes? Should you try Ashtanga or Hatha? And what's the difference between Dynamic yoga and Vinyasa? The array of options can be enough to scare newbies off the mat for good.
But here's why you shouldn't be scared: Like cross training, incorporating a variety of types of yoga into your regular practice can help keep you balanced. Try a few different levels, teachers and styles. Then, stick with the one that resonates with you for a good amount of time and be dedicated to the practice.</h6>

                        <p class="wow fadeInRight" data-wow-delay="0.2s">
Yoga isn't necessarily a 'one-size-fits-all' practice. Different types of yoga might be best for different people. Depending on your age, your flexibility or stiffness there is the right class and level for you. Our Yogi's will guide you in identifying the style you might like best.</p>
                        <ul class="info-list">
                            <li class="english wow fadeInRight" style="display:none" data-wow-delay="0.4s"><a href="#">- Be early for your class (at least 5-10 minutes in advance)</a></li>
                            <li class="english wow fadeInRight" style="display:none" data-wow-delay="0.4s"><a href="#">- Your mobile phone is to be kept outside of the studio room to ensure the<BR>&nbsp;&nbsp;&nbsp;most peaceful hour of practice for yourself and others</a></li>
                            <li class="english wow fadeInRight" style="display:none" data-wow-delay="0.4s"><a href="#">- Keep the studio clean when you leave</a></li>
                            
                            <li class="vietnam wow fadeInRight" style="display:none" data-wow-delay="0.4s"><a href="#">- Đến lớp trước giờ bắt đầu ít nhất 5 phút</a></li>
                            <li class="vietnam wow fadeInRight" style="display:none" data-wow-delay="0.4s"><a href="#">- Tắt điện thoại nhằm đảm bảo không gian yên tĩnh</a></li>
                            <li class="vietnam wow fadeInRight" style="display:none" data-wow-delay="0.4s"><a href="#">- Giữ gìn không gian luyện tập sạch đẹp</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </section>



	<div id="cd-schedule" class="cd-schedule loading">
	<div class="timeline">
		<ul>
			<li><span>06:00</span></li>
			<li><span>06:30</span></li>
			<li><span>07:00</span></li>
			<li><span>07:30</span></li>		
			<li><span>08:00</span></li>
			<li><span>08:30</span></li>
			<li><span>09:00</span></li>
			<li><span>09:30</span></li>
			<li><span>10:00</span></li>
			<li><span>10:30</span></li>
			<li><span>11:00</span></li>
			<li><span>11:30</span></li>
			<li><span>12:00</span></li>
			<li><span>12:30</span></li>
			<li><span>13:00</span></li>
			<li><span>13:30</span></li>
			<li><span>14:00</span></li>
			<li><span>14:30</span></li>
			<li><span>15:00</span></li>
			<li><span>15:30</span></li>
			<li><span>16:00</span></li>
			<li><span>16:30</span></li>
			<li><span>17:00</span></li>
			<li><span>17:30</span></li>
			<li><span>18:00</span></li>
			<li><span>18:15</span></li>
			<li><span>19:15</span></li>
			<li><span>19:30</span></li>
			<li><span>20:30</span></li>
		</ul>
	</div> <!-- .timeline -->

	<div class="events">
		<ul>
			<li class="events-group">
				<div class="top-info"><span>Monday</span></div>

				<ul>
					<li class="single-event" data-start="06:30" data-end="07:30" data-content="Flow-Yoga" data-event="event-1">
						<a href="#0">
							<em class="event-name">Flow yoga<BR>(VIE)<BR>Ms. Trang</em>
						</a>
					</li>

					<li class="single-event" data-start="08:30" data-end="09:30" data-content="Hatha-Yoga" data-event="event-1">
						<a href="#0">
							<em class="event-name">Hatha Yoga<BR>(ENG/KOR)<BR>Ms. Kim</em>
						</a>
					</li>

					<li class="single-event" data-start="12:00" data-end="13:00"  data-content="Hatha-Yoga" data-event="event-1">
						<a href="#0">
							<em class="event-name">Hatha Yoga<BR>(ENG/VIE)<BR>Ms. Jiva</em>
						</a>
					</li>
					<li class="single-event" data-start="15:00" data-end="16:00"  data-content="Hatha-Yoga" data-event="event-1">
						<a href="#0">
							<em class="event-name">Hatha Yoga<BR>(ENG/VIE)<BR>Ms. Nhu</em>
						</a>
					</li>
					<li class="single-event" data-start="17:00" data-end="18:00"  data-content="Dynamic-Yoga" data-event="event-1">
						<a href="#0">
							<em class="event-name">Dynamic Yoga<BR>(VIE)<BR>Ms. Phu</em>
						</a>
					</li>
					<li class="single-event" data-start="18:15" data-end="19:15"  data-content="Hatha-Yoga" data-event="event-1">
						<a href="#0">
							<em class="event-name">Hatha Yoga<BR>(VIE)<BR>Ms. Phu</em>
						</a>
					</li>					
				</ul>
			</li>

			<li class="events-group">
				<div class="top-info"><span>Tuesday</span></div>

				<ul>

					<li class="single-event" data-start="07:30" data-end="08:30"  data-content="Hatha-Yoga" data-event="event-2">
						<a href="#0">
							<em class="event-name">Hatha Yoga<BR>(ENG/VIE)<BR>Ms. Jiva</em>
						</a>
					</li>

					<li class="single-event" data-start="12:00" data-end="13:00" data-content="Vinyasa-Yoga" data-event="event-2">
						<a href="#0">
							<em class="event-name">Vinyasa Yoga<BR>(ENG/VIE)<BR>Ms. Nhu</em>
						</a>
					</li>

					<li class="single-event" data-start="15:00" data-end="16:00"  data-content="Flow-Yoga" data-event="event-2">
						<a href="#0">
							<em class="event-name">Flow Yoga<BR>(ENG/VIE)<BR>Ms. Tuan Anh</em>
						</a>
					</li>
					<li class="single-event" data-start="17:00" data-end="18:00"  data-content="Flow-Yoga" data-event="event-2">
						<a href="#0">
							<em class="event-name">Fit Flow Yoga<BR>(ENG/VIE)<BR>Ms. Nhu</em>
						</a>
					</li>
					<li class="single-event" data-start="18:15" data-end="19:15"  data-content="Vinyasa-Yoga" data-event="event-2">
						<a href="#0">
							<em class="event-name">Vinyasa Yoga<BR>(ENG/VIE)<BR>Ms. Linh</em>
						</a>
					</li>	
					<li class="single-event" data-start="19:30" data-end="20:30"  data-content="Hatha-Yoga" data-event="event-2">
						<a href="#0">
							<em class="event-name">Hatha Yoga<BR>(ENG/VIE)<BR>Ms. Linh</em>
						</a>
					</li>					
				</ul>
			</li>

			<li class="events-group">
				<div class="top-info"><span>Wednesday</span></div>

				<ul>
					<li class="single-event" data-start="06:30" data-end="07:30" data-content="Hatha-Yoga" data-event="event-1">
						<a href="#0">
							<em class="event-name">Hatha Yoga<BR>(VIE)<BR>Ms. Trang</em>
						</a>
					</li>

					<li class="single-event" data-start="08:30" data-end="09:30" data-content="Vinyasa-Yoga" data-event="event-1">
						<a href="#0">
							<em class="event-name">Vinyasa Yoga<BR>(ENG/KOR)<BR>Ms. Kim</em>
						</a>
					</li>

					<li class="single-event" data-start="12:00" data-end="13:00"  data-content="Hatha-Yoga" data-event="event-1">
						<a href="#0">
							<em class="event-name">Hatha Yoga<BR>(ENG/VIE)<BR>Ms. Jiva</em>
						</a>
					</li>
					<li class="single-event" data-start="17:00" data-end="18:00" data-content="Ashtanga-Yoga" data-event="event-1">
						<a href="#0">
							<em class="event-name">Ashtanga Yoga<BR>(VIE)<BR>Ms. Phu</em>
						</a>
					</li>	
					<li class="single-event" data-start="18:15" data-end="19:15" data-content="Dynamic-Yoga" data-event="event-1">
						<a href="#0">
							<em class="event-name">Dynamic Yoga<BR>(VIE)<BR>Ms. Phu</em>
						</a>
					</li>					
				</ul>
			</li>

			<li class="events-group">
				<div class="top-info"><span>Thursday</span></div>

				<ul>
					<li class="single-event" data-start="07:30" data-end="08:30"  data-content="Ashtanga-Yoga" data-event="event-2">
						<a href="#0">
							<em class="event-name">Ashtanga Yoga<BR>(ENG/VIE)<BR>Ms. Jiva</em>
						</a>
					</li>

					<li class="single-event" data-start="12:00" data-end="13:00" data-content="Vinyasa-Yoga" data-event="event-2">
						<a href="#0">
							<em class="event-name">Vinyasa Yoga<BR>(ENG/VIE)<BR>Ms. Nhu</em>
						</a>
					</li>

					<li class="single-event" data-start="15:00" data-end="16:00"  data-content="Flow-Yoga" data-event="event-2">
						<a href="#0">
							<em class="event-name">Flow Yoga<BR>(ENG/VIE)<BR>Ms. Tuan Anh</em>
						</a>
					</li>
					<li class="single-event" data-start="17:00" data-end="18:00"  data-content="Hatha-Yoga" data-event="event-2">
						<a href="#0">
							<em class="event-name">Hatha Yoga<BR>(ENG/VIE)<BR>Ms. Nhu</em>
						</a>
					</li>
					<li class="single-event" data-start="18:15" data-end="19:15"  data-content="Vinyasa-Yoga" data-event="event-2">
						<a href="#0">
							<em class="event-name">Vinyasa Yoga<BR>(ENG/VIE)<BR>Ms. Linh</em>
						</a>
					</li>	
					<li class="single-event" data-start="19:30" data-end="20:30"  data-content="Hatha-Yoga" data-event="event-2">
						<a href="#0">
							<em class="event-name">Hatha Yoga<BR>(ENG/VIE)<BR>Ms. Linh</em>
						</a>
					</li>					
				</ul>
			</li>

			<li class="events-group">
				<div class="top-info"><span>Friday</span></div>

				<ul>
					<li class="single-event" data-start="06:30" data-end="07:30" data-content="Flow-Yoga" data-event="event-1">
						<a href="#0">
							<em class="event-name">Flow Yoga<BR>(VIE)<BR>Ms. Trang</em>
						</a>
					</li>

					<li class="single-event" data-start="08:30" data-end="09:30" data-content="Hatha-Yoga" data-event="event-1">
						<a href="#0">
							<em class="event-name">Hatha Yoga<BR>(ENG/KOR)<BR>Ms. Kim</em>
						</a>
					</li>

					<li class="single-event" data-start="12:00" data-end="13:00"  data-content="Power-Yoga" data-event="event-1">
						<a href="#0">
							<em class="event-name">Power Yoga<BR>(ENG/VIE)<BR>Ms. Jiva</em>
						</a>
					</li>

					<li class="single-event" data-start="15:00" data-end="16:00" data-content="Hatha-Yoga" data-event="event-1">
						<a href="#0">
							<em class="event-name">Hatha Yoga<BR>(ENG/VIE)<BR>Ms. Nhu</em>
						</a>
					</li>
					<li class="single-event" data-start="17:00" data-end="18:00" data-content="Vinyasa-Yoga" data-event="event-1">
						<a href="#0">
							<em class="event-name">Vinyasa Yoga<BR>(ENG/VIE)<BR>Ms. Jiva</em>
						</a>
					</li>	
					<li class="single-event" data-start="18:15" data-end="19:15" data-content="Flow-Yoga" data-event="event-1">
						<a href="#0">
							<em class="event-name">Fit Flow Yoga<BR>(ENG/VIE)<BR>Ms. Nhu</em>
						</a>
					</li>					
				</ul>
			</li>
			<li class="events-group">
				<div class="top-info"><span>Saturday</span></div>

				<ul>
					<li class="single-event" data-start="08:00" data-end="09:00" data-content="Hatha-Yoga" data-event="event-2">
						<a href="#0">
							<em class="event-name">Hatha Yoga<BR>(VIE)<BR>Mr. Tuan Anh</em>
						</a>
					</li>

					<li class="single-event" data-start="09:30" data-end="10:30"  data-content="Kid-Yoga" data-event="event-2">
						<a href="#0">
							<em class="event-name">Yoga for Kids<BR>(VIE)<BR>Mr. Tuan Anh</em>
						</a>
					</li>
				
				</ul>
			</li>
						<li class="events-group">
				<div class="top-info"><span>Sunday</span></div>

				<ul>
					<li class="single-event" data-start="08:00" data-end="09:00" data-content="Hatha-Yoga" data-event="event-1">
						<a href="#0">
							<em class="event-name">Hatha Yoga<BR>(VIE)<BR>Ms. Phu</em>
						</a>
					</li>

					<li class="single-event" data-start="09:30" data-end="10:30"  data-content="Ashtanga-Yoga" data-event="event-1">
						<a href="#0">
							<em class="event-name">Ashtanga for Kids<BR>(VIE)<BR>Ms. Phu</em>
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</div>

	<div class="event-modal">
		<header class="header">
			<div class="content">
				<span class="event-date"></span>
				<h3 class="event-name"></h3>
			</div>

			<div class="header-bg"></div>
		</header>

		<div class="body">
			<div class="event-info"></div>
			<div class="body-bg"></div>
		</div>

		<a href="#0" class="close">Close</a>
	</div>

	<div class="cover-layer"></div>
</div> <!-- .cd-schedule -->
<script src="js/modernizr.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script>
	if( !window.jQuery ) document.write('<script src="js/jquery-3.0.0.min.js"><\/script>');
</script>
<script src="js/main.js"></script> <!-- Resource jQuery -->

           
    </main>
	

	
<?php include 'Footer.php';?>

</div>

<script src="js/script.js"></script>
</body>
</html>