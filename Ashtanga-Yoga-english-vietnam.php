
<div class="event-info">
                        <div>
                        Ashtanga là một sự kết hợp giữa các tư thế của cơ thể được thực hiện bằng cách phối hợp đều đặn từng nhịp thở và động tác. Mỗi động tác đi kèm một nhịp thở, đều đặn trong khoảng một giờ đồng hồ và được sự kết thúc bằng sự thư giãn hoàn toàn. Điểm quan trọng nhất của Ashtanga Yoga là nó cho phép ta ý thức được nhịp thở của mình.<BR><BR>Ashtanga không chỉ giúp người tập xua đi sự mệt mỏi, lấy lại sự thanh bình trong tâm hồn mà nó còn giúp cho cơ thể hình thành cơ bắp săn chắc. Phương pháp này được ví như sự kết hợp giữa sức mạnh , sự mềm dẻo, sự ổn định. Sự lưu thông khí huyết có vai trò rất quan trọng trong việc giữ gìn tuổi thanh xuân cho phụ nữ hay đảm bảo sinh lực cho nam giới.</div>                    
</div>
