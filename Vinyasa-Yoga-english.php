<div class="event-info">
                        <div>
                        Vinyasa Yoga connects one posture to the next using breath. Vinyasa is a style of yoga characterized by combining varied postures together so that you move from one to another, seamlessly, using breath.<BR><BR>Commonly referred to as “flow yoga” Vinyasa classes offer a variety of postures and no two classes are ever alike.<BR><BR>Vinyasa yoga offers much diversity. The pace can vary and there is no particular sequence that instructors must follow. This flexibility allows the instructor to tailor the sequence to their own philosophy.<BR><BR>So, if one class doesn't work for you, just try another, until you can find one that you are comfortable with.</div>
</div>
