<?php
$home = 'class="active"';
$about = '';
$classes = '';
$news = '';
$contact = '';
$gallery = '';
?>
<!doctype html>
<html lang="en">
<head>
    <title>Home</title>
<?php include 'Header.php';?>
    <!--========================================================
                              CONTENT
    =========================================================-->
    <main>
        <section>
            <div class="camera_container">
                <div id="camera" class="camera_wrap">
                    <div data-src="images/page-1_slide01.jpg">
                        <div class="camera_caption fadeIn">
                            <div class="container center">
                                <div class="row">
                                    <div class="preffix_2 grid_8">
                                        <h3 class="english">Breathe new life into your practices</h3>
                                        <h3 class="vietnam">Làn gió mới cho những giờ luyện tập</h3>
                                        <!-- <a class="btn" href="#">read more</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-src="images/page-1_slide02.jpg">
                        <div class="camera_caption fadeIn">
                            <div class="container center">
                                <div class="row">
                                    <div class="preffix_2 grid_8">
                                        <h3 class="english">Fresh air from nature for souls who loves Yoga</h3>
                                        <h3 class="vietnam">Hơi thở mát lành từ thiên nhiên cho những tâm hồn yêu Yoga</h3>
                                        <!-- <a class="btn" href="#">read more</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-src="images/page-1_slide03.jpg">
                        <div class="camera_caption fadeIn">
                            <div class="container center">
                                <div class="row">
                                    <div class="preffix_2 grid_8">
                                        <h3 class="english">Relaxing moments at the heart of Thao Dien district 2</h3>
                                        <h3 class="vietnam">Khoảnh khắc thư giãn ngay giữa lòng Thảo Điền Quận 2</h3>
                                        <!-- <a class="btn" href="#">read more</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="well">
            <div class="container">
                <h4 class="english center secondary-color">Special benefits of Yoga Pod</h4>
                <h4 class="vietnam center secondary-color">Những tiện ích đặc biệt của Yoga Pod</h4>
                <h6 class="english center">A green and open space where your respiration is taken care of</h6>
                <h6 class="vietnam center">AKhông gian xanh mát và trong lành trọn vẹn cho từng hơi thở an nhiên</h6>

                <ul class="row index-list">
                    <li class="grid_4 wow fadeInRight" data-equal-group="1" >
                        <div class="wrapp">
                            <h6 class="english">A peaceful secret garden</h6>
                            <h6 class="vietnam">Khu vườn bí mật yên tĩnh<BR><BR></h6>
                            <p class="english">First time ever in Saigon, a space to practice Yoga is purposefully designed within a verdant, pure and silent garden </p>
                            <p class="vietnam">Lần đầu tiên tại Sài Gòn, không gian luyện tập yoga được thiết kế theo hướng khu vườn xanh mát, trong lành và an yên </p>
                        </div>
                    </li>

                    <li class="grid_4 wow fadeInRight" data-equal-group="1" data-wow-delay="0.2s">
                        <div class="wrapp">
                          <h6 class="english">Everyone in your family can enjoy yoga</h6>
                          <h6 class="vietnam">Mọi thành viên trong gia đình đều có thể tận hưởng yoga</h6>
                          <p class="english">You can easily book a mat for your kids; special session for pregnancy and private classes at home with professional trainers</p>
                          <p class="vietnam">Bạn hoàn toàn dễ dàng đăng ký lớp học dành riêng cho trẻ em, mẹ bầu và lớp riêng tại nhà với các huấn luyện viên chuyên nghiệp</p>
                        </div>
                    </li>
                    <li class="grid_4 wow fadeInRight" data-equal-group="1" data-wow-delay="0.4s">
                        <div class="wrapp">
                            <h6 class="english">Newbies are highly welcomed</h6>
                            <h6 class="vietnam">Người mới bắt đầu làm quen yoga luôn được chào đón</h6>
                            <p class="english">Don’t be afraid of trying out yoga as you can always find your customized classes at the first attempt and our packages are all short-term</p>
                            <p class="vietnam">Vì bạn có thể dễ dàng tìm được các lớp yoga phù hợp dành riêng cho lần thử sức đầu tiên cũng như qua các gói tập ngắn hạn của Yoga Pod
</p>
                        </div>
                    </li>
  <!--                  <li class="grid_3 wow fadeInRight" data-equal-group="1" data-wow-delay="0.6 wows">
                        <div class="wrapp">
                            <h6>Accountability </h6>

                            <p>Elementum velitIpede mi, aliquet sit ametuismodIn pede mi, aliquet
                                sit amet, euismod
                                in,auctor ut, ligulaLorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent
                                vestibulum molestie lacenean</p>
                        </div>
                    </li>
  -->                  
                </ul>
            </div>
        </section>
   <!-- <section class="well well__ins1 parallax parallax1" data-url="images/parallax1.jpg" data-mobile="false"
                 data-speed="0.5">
            <div class="container center">
                <h4>news & events</h4>

                <div class="row">
                    <div class="grid_4 wow fadeInLeft">
                        <div class="img_wrapp"><img src="images/page-1_img01.jpg" alt=""/></div>
                        <p class="primary-color">Elementum velitIpede mi, aliquet sit ametuis modIn pede mi, aliquet sit
                            amet, euismod </p>
                        <hr/>
                        <time datetime="2015">May 15, 2015</time>
                    </div>
                    <div class="grid_4 wow fadeIn" data-wow-duration="0.5s">
                        <div class="img_wrapp"><img src="images/page-1_img02.jpg" alt=""/></div>
                        <p class="primary-color">Elementum velitIpede mi, aliquet sit ametuis modIn pede mi, aliquet sit
                            amet, euismod </p>

                        <hr/>
                        <time datetime="2015">May 22, 2015</time>
                    </div>
                    <div class="grid_4 wow fadeInRight">
                        <div class="img_wrapp"><img src="images/page-1_img03.jpg" alt=""/></div>
                        <p class="primary-color">Elementum velitIpede mi, aliquet sit ametuis modIn pede mi, aliquet sit
                            amet, euismod </p>

                        <hr/>
                        <time datetime="2015">May 31, 2015</time>
                    </div>
                </div>
                <a class="btn" href="#">go to blog</a>
            </div>
        </section>
        <section class="well well__ins2 bg-primary">
            <div class="container">
                <div class="row">
                    <div class="preffix_6 grid_6">
                        <h2>Regular training
                            makes your body
                            sound and your
                            soul strong</h2>

                        <p>Elementum velitIpede mi, aliquet sit ametuismodIn pede mi, ali quet sit amet, euismod
                            in,auctor ut, ligulaLorem ipsum dolor sit amet, consectetuer adipiscing eliaesent
                            vestibulum.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="well well__ins3">
            <div class="container center">
                <div class="row">
                    <div class="preffix_1 grid_10">

                        <h4 class="secondary-color wow fadeInLeft">Prompt and trustworthy sport information</h4>
                        <h6 class="wow fadeInRight" data-wow-delay="0.2s">Elementum velitIpede mi, aliquet sit ametuismodIn pede mi, aliquet sit amet, euismod
                            in,auctor
                            ut,
                            ligulaLorem ipsum dolor sit amet, consectetuer adipiscing elit. </h6>

                        <p class="primary-color wow fadeInLeft" data-wow-delay="0.4s">Elementum velitIpede mi, aliquet sit ametuismodIn pede mi, aliquet sit
                            amet, euismod in,auctor ut,
                            ligulaLorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie
                            lacenean nonummyhendrerit mauris. </p>

                        <p class="wow fadeInRight" data-wow-delay="0.6s">
                            Phasellus porta. Fusce suscipit varius mi. Lorem ipsum dolor sit amet, consectetuer
                            adipiscing
                            elit.
                            Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce
                            suscipit
                            varius mi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum
                            molestie
                            lacus. Morbi nunc odio, gravida at, cursus nec, luctus a, lorem. Maecenas tristique orci ac
                            sem.
                            Duis ultricies pharetra </p>
                        <a class="btn" href="#">contact me now</a></div>
                </div>
            </div>
        </section>
        <section class="well well__ins4 parallax parallax2" data-url="images/parallax2.jpg" data-mobile="false"
                 data-speed="1">
            <div class="container center">
                <div class="row">
                    <div class="preffix_1 grid_10">
                        <h4>Testimonials</h4>

                        <div class="owl-carousel">
                            <div class="item">
                                <blockquote>
                                    <p><q>Elementum velitIpede mi, aliquet sit ametuismodIn pede mi, aliquet sit amet,
                                        euismod
                                        in,auctor ut, <br/> ligula orem ipsum dolor sit amet, consectetuer adipiscing
                                        elit. </q></p>
                                    <h6><cite>- Dan Adams</cite></h6>
                                </blockquote>

                            </div>
                            <div class="item">
                                <blockquote>
                                    <p><q>Elementum velitIpede mi, aliquet sit ametuismodIn pede mi, aliquet sit amet,
                                        euismod
                                        in,auctor ut, <br/> ligula orem ipsum dolor sit amet, consectetuer adipiscing
                                        elit. </q></p>
                                    <h6><cite>- Alex Smith</cite></h6>
                                </blockquote>
                            </div>
                            <div class="item">
                                <blockquote>
                                    <p><q>Elementum velitIpede mi, aliquet sit ametuismodIn pede mi, aliquet sit amet,
                                        euismod in,auctor ut, <br/> ligula orem ipsum dolor sit amet, consectetuer
                                        adipiscing
                                        elit. </q></p>
                                    <h6><cite>- Anna Anna</cite></h6>
                                </blockquote>
                            </div>
                            <div class="item">
                                <blockquote>
                                    <p><q>Elementum velitIpede mi, aliquet sit ametuismodIn pede mi, aliquet sit amet,
                                        euismod in,auctor ut, <br/> ligula orem ipsum dolor sit amet, consectetuer
                                        adipiscing
                                        elit. </q></p>
                                    <h6><cite>- Ivan Ivanov</cite></h6>
                                </blockquote>
                            </div>
                            <div class="item">
                                <blockquote>
                                    <p><q>Elementum velitIpede mi, aliquet sit ametuismodIn pede mi, aliquet sit
                                        amet, euismod in,auctor ut, <br/> ligula orem ipsum dolor sit amet, consectetuer
                                        adipiscing elit. </q></p>
                                    <h6><cite>-Man Untd</cite></h6>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="nav-list">
                    <li><a class="dot active"><img src="images/page-1_img04.jpg" alt=""/></a>
                    </li>
                    <li><a class="dot"><img src="images/page-1_img05.jpg" alt=""/></a></li>
                    <li><a class="dot"><img src="images/page-1_img06.jpg" alt=""/></a></li>
                    <li><a class="dot"><img src="images/page-1_img07.jpg" alt=""/></a></li>
                    <li><a class="dot"><img src="images/page-1_img08.jpg" alt=""/></a></li>
                </ul>
            </div>
        </section>
  -->         
    </main>
<?php include 'Footer.php';?>
</div>

<script src="js/script.js"></script>
</body>
</html>