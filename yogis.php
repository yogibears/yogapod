<?php
$yogis = 'class="active"';
?>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Yoga Pod - Instructors</title>

<?php
$lang = $_GET['lang'];
if ($lang == "" or $lang == "english") {
    $lang = "english";
} else {
    $lang = "vietnam";
}
?>

    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no"/>
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/camera.css"/>
    <link rel="stylesheet" href="css/owl-carousel.css"/>
<!-- Add jQuery library  -->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="/fancybox/lib/jquery.mousewheel.pack.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="/fancybox/source/jquery.fancybox.css?v=2.1.7" type="text/css" media="screen" />
<script type="text/javascript" src="/fancybox/source/jquery.fancybox.pack.js?v=2.1.7"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>	


<script src='js/device.min.js'></script>

<?php include 'Header.php';?>

    <!--========================================================
                              CONTENT
    =========================================================-->
    <main>
        <section class="well well__ins5">
            <div class="container center">
                <h4 class="secondary-color">Instructors</h4>
                <h6>Integer rutrum ante eu lacus. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                    Praesent vestibulum molestie lacus. Aenean nonummy.</h6>

                <div class="row">
                    <div class="grid_4 wow fadeInRight" data-wow-delay="0.4s" ><a class="thumb fancybox" href="#jiva" data-fancybox-group="1"><img class="yogi" src="yogis/Jiva_thumb.jpg" alt=""/></a><p class="primary-color padding-mod">Yogi Jiva<BR>Vinyassa, Hatha and Ashtanga Yoga</p></div>
                    <div class="grid_4 wow fadeInRight" data-wow-delay="0.2s" ><a class="thumb fancybox" href="#linh" data-fancybox-group="1"><img class="yogi" src="yogis/Linh_thumb.jpg" alt=""/></a><p class="primary-color padding-mod">Linh<BR>Hatha, Vinyasa with a touch of Iyengar</p></div>
                    <div class="grid_4 wow fadeInRight" ><a class="thumb fancybox" href="#nhu" data-fancybox-group="1"><img class="yogi" src="yogis/Nhu_thumb.jpg" alt=""/></a><p class="primary-color padding-mod">Nhu<BR>Hatha, Vinyasa flow, Power yoga, Interval yoga classes and Ashtanga</p></div>
                </div>
                <div class="row">
                    <div class="grid_4 wow fadeInRight" ><a class="thumb fancybox" href="#phu" data-fancybox-group="1"><img class="yogi" src="yogis/Phu_thumb.jpg" alt=""/></a><p class="primary-color padding-mod">Phu<BR>Hatha, Vinyasa flow, Power yoga, Interval yoga classes and Ashtanga</p></div>
                    <div class="grid_4 wow fadeInRight" data-wow-delay="0.2s"><a class="thumb" href="images/page-4_img05_original.jpg" data-fancybox-group="1"><img src="images/page-4_img05.jpg" alt=""/><span class="thumb_overlay"></span></a><p class="primary-color padding-mod">Praesent vestibulum molestie lacus. Aenean nonummy hendrerit.</p></div>
                    <div class="grid_4 wow fadeInRight" data-wow-delay="0.4s"><a class="thumb" href="images/page-4_img06_original.jpg" data-fancybox-group="1"><img src="images/page-4_img06.jpg" alt=""/><span class="thumb_overlay"></span></a><p class="primary-color padding-mod">Praesent vestibulum molestie lacus. Aenean nonummy hendrerit.</p></div>
                </div>
            </div>
			

        </section>

    </main>

</div>
<div id="jiva" style="display: none;">
		<img src="yogis/Jiva.jpg"><BR><BR>
		<h4>Yogi Jiva</h4><h6>
							  I have been practicing yoga for ten years, teaching for 3 years.<BR>
							  I have received my 200 hour training certification at Rishikesh Yog Peeth, in northern India.<BR><BR>
							  In my classes I like to weave Vinyassa, Hatha and Ashtanga yoga together to cultivate a deep<BR>
							  connection to the breath and movement, while helping the body find deep release and<BR>
							  the mind a place of tranquility.</h6>
</div>
<div id="linh" style="display: none;">
		<img src="yogis/Linh.jpg"><BR><BR>
		<h4>Linh</h4><h6>Found yoga over 5 years ago when I had to go through one of the biggest shocks of my life and have got<BR>
						 seriously into it since 2014. Since then, it has tremendously improved my life quality, physically and mentally.<BR>
						 Thanks to yoga, I also found my other interests in trekking and diving, which brings my health up to another level.</h6>
						 <span style="font-style: italic;"><BR>I found peace of mind and never feel the need of compare myself to others.<BR><BR></span>
						 <h6>In order to inspire more people to touch the beauty of yoga, I went to India for a 200hr YTC course at<BR>
						 Shiva Yoga Peeth in Rishikesh, India in 2016. Hatha, Vinyasa with a touch of Iyengar to ensure your yoga alignment<BR>
						 and safety is my teaching philosophy.</h6>
</div>	
<div id="nhu" style="display: none;">
		<img src="yogis/Nhu.jpg"><BR><BR>
		<h4>Nhu</h4><h6>
							  Yoga Alliance Registered 200YRT Certification, Shiva Yoga Peeth-Rishikesh-India.<BR>
							  I tend to teach in a gentle, joyful manner and bring a good deal of fantasy and playfulness into my classes. My approach encourages body<BR>
							  awareness, a sense of exploration and independence in the practice ('body intelligence'). I focus on the breathing and lead the students<BR>
							  throughout the sequences to the centre life of their body and their mind.<BR>
							  I teach Hatha, Vinyasa flow, Power yoga, interval yoga classes. Creative classes mixing all three types. Ashtanga modified led primary series</h6>
							  <BR><span style="font-style: italic;">If you practice yoga once a week, you will change you body.<BR>
							  If you practice yoga twice a week, you will change your mind.<BR>
							  But if you practice yoga every day, you will change your life.<BR><BR></span>	
							  <h6>Yoga has changed my life, it brings me the passion, and the freedom I have always wished for in my life. I believe in the transfomation,<BR>
							  healing, restorative and uplifting power of yoga, I am driven to support and encourage people to step on the mat and keep coming back.<BR>
							  I feel that by doing that, I am making a positive difference.</h6>
</div>	

<div id="phu" style="display: none;">
		<img src="yogis/Phu.jpg"><BR><BR>
		<h4>Phu</h4><h6>
							  Academic Diver Course Basic and Advanced Yoga Instructor of Ho Chi Minh City Gymnastics Association. Ho Chi Minh City in 2013</h6>
							  <span style="font-style: italic;"><BR>
								- Third prize in Yoga Prize. Ho Chi Minh City expands in 2015<BR>
								- First Prize in Yoga Prize. Ho Chi Minh expands - 2nd Viet A Cup in 2016<BR>
								- Many provincial and national awards for other sports.<BR><BR></span>	
								<h6>2-year experience teaching sports subjects at Shanxi High School, University of Industry; Over 4 years teaching yoga at many major<BR>
								centers in Ho Chi Minh City. TDC Health And Beauty, Lee Gym & Yoga Center, TD Gymnasium ,Green Garden Fitness Center (Thu Duc)<BR>
								Education - Graduation from University of Pedagogy and Sport of HCMC. </h6>
								<span style="font-style: italic;"><BR>
								- Master Degree in Physical Education - Kind of excellence<BR>
								- Advanced Yoga Coach Certification - Excellent type by the Ho Chi Minh City Gymnastics Federation<BR>
								- Aerobic Instructor Certificate issued by the College of Education.<BR>
								- Certificate of Fitness Instructor by Ho Chi Minh City Gymnastics Federation<BR><BR></span>	
								<h6>A professional athlete from a young age, with the will and personality to be involved in a lot of sports.<BR>
								Accidentally came across Yoga through the introduction of two University Lecturers.<BR>
								I discovered a great workout I've been looking for.
</h6>
</div>

<?php include 'Footer.php';?>

<script src="js/script.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".fancybox").fancybox();
	});
</script>

</body>
</html>