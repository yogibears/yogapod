# README #

## GIT Setup ##

1. Download GIT from https://git-scm.com/downloads or https://tortoisegit.org/ if you are using Windows OS
2. Run "git clone https://swish_technology@bitbucket.org/yogibears/yogapod.git" to clone the repository to your local directory.

## Pushing your changes to the remote repository (Bitbucket) ##

1. Run "git add ." to stage all your changes
2. Run "git commit" to commit your changes
3. Enter a commit message
3. Enter the vi command ":wq!" to save and close
4. Run "git push" to push the changes to the remote repository

## Pulling changes from the remote repository (Bitbucket) ##

1. Run "git pull" to pull all changes from the remote reposity