<?php
$home = '';
$about = '';
$classes = '';
$news = '';
$contact = '';
$gallery = '';
?>
<html lang="en">
<head>
    <title>Yoga Pod - Privacy Policy</title>


	
<?php
$lang = $_GET['lang'];
if ($lang == "" or $lang == "english") {
    $lang = "english";
} else {
    $lang = "vietnam";
}
?>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no"/>
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/camera.css"/>
    <link rel="stylesheet" href="css/owl-carousel.css"/>
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script>

    <!--[if lt IE 9]>
    <html class="lt-ie9">
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/..">
            <img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"
                 alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
        </a>
    </div>
    <script src="js/html5shiv.js"></script>
    <![endif]-->

    <script src='js/device.min.js'></script>	

	
<?php include 'Header.php';?>
    <!--========================================================
                              CONTENT
    =========================================================-->
    <main>
        <section class="well well__ins4">
            <div class="container">
                <h1 class="center">PRIVACY POLICY</h1>
                <dl class="terms-list">
					<p>This Privacy Policy applies to the collection, use and disclosure of an individual customer's Personal Data (hereinafter defined) arising from goods and/or services offered by Yoga Pod Vietnam ("Yoga Pod").</p>
					<p><strong>1. General</strong></p>
					<p>1.1 This Policy statement provides information on the obligations and policies of Yoga Pod in respect of an individual customer's Personal Data. Yoga Pod undertakes to use reasonable efforts in applying, where practicable, those principles and the processes set out herein to its operations.</p>
					<p>1.2 Yoga Pod's management and members of staff shall use reasonable endeavours to respect the confidentiality of and keep safe any and all Personal Data collected and/or stored and/or disclosed and/or used for, or on behalf of, Yoga Pod. Yoga Pod shall use reasonable endeavours to ensure all collection and/or storage and/or disclosure and/or usage of Personal Data by Yoga Pod shall be done in an appropriate manner and in accordance to the Act and this Policy.</p>
					<p><br /><strong>2. Contacting the Data Protection Officer</strong></p>
					<p>2.1 Where you legitimately request access to and/or correction of Personal Data relating to you which is in the possession and control of Yoga Pod, Yoga Pod shall provide and/or correct that data in a reasonable time and manner in accordance with its standard procedures as stated hereinafter.</p>
					<p>2.2 In accordance with the Act, Yoga Pod has established a process for receiving and responding to any query or complaint that may arise with respect to the application of this Act. To ensure that Yoga Pod receives your complaints and enquiries, please send the same via email to info@yogapodsaigon.com.</p>
					<p>2.3 Should you not wish Yoga Pod to use your Personal Data for any of the purposes listed in Clauses 3.2 to 3.4, or not to receive promotional materials from Yoga Pod, you may opt out by sending a clearly worded email to the email address provided in Clause 2.2.  Your request shall be processed within a reasonable time.</p>
					<p><strong>3. Statement of Practices</strong></p>
					<p>Types of Personal Data Collected</p>
					<p>3.1 As part of its day to day activities, Yoga Pod may collect from you, through various means, including via our website, from time to time, the following Personal Data: name (first and surname); postal address; phone number (including mobile); fax number; e-mail address; gender; Personal Data of your emergency contacts; photographs and images.</p>
					<p>We use cookies on our websites to track website visitorship and experience. Most web browsers are designed to accept cookies. If you do not wish to receive any cookies, you may set your browser to refuse it.</p>
					<p>Purpose of Collection of Personal Data</p>
					<p>3.2 The above Personal Data mentioned in Clause 3.1 is collected for the purposes of processing  your application and registration of your membership and to ascertain if you are eligible for discounts, privileges or benefits or other related purposes; to conduct market research and analysis; for direct marketing through voice calls; text messages; email; direct mail and facsimile messages; to notify you of any changes to our policies or services which may affect you; to respond to queries and feedback; for identification and club access; maintaining and updating your membership details; and informing you of new developments, services, promotions of Yoga Pod and other third parties which we are associated with.</p>
					<p>Disclosure of Personal Data</p>
					<p>3.3 Yoga Pod will not, in any way, disclose your Personal Data unless they are for regulators and law enforcement officials; lawyers; auditors; third party service providers and consultants; third party investors; credit, debit and charge card companies, banks and other entities processing paYoga Podent; and any agent or subcontractor acting on Yoga Pod's behalf for the provision of Yoga Pod's services.</p>
					<p><strong>4. Transfer of Personal Data Overseas</strong></p>
					<p>Your Personal Data may be processed by Yoga Pod in jurisdictions outside of Vietnam.  In this event Yoga Pod will comply with the terms of the Act.</p>
					<p><strong>5. Accuracy of Personal Data</strong></p>
					<p>Where possible, Yoga Pod will validate data provided using generally accepted practices and guidelines. This includes the use of check sum verification on some numeric fields such as account numbers or credit card numbers. In some instances, Yoga Pod is able to validate the data provided against pre-existing data held by Yoga Pod. In some cases, Yoga Pod is required to see original documentation before we may use the Personal Data such as with Personal Identifiers and/or proof of address. To assist in ensuring the accuracy of your Personal Data in the possession of Yoga Pod, please inform us of any updates of any parts of your Personal Data by sending a clearly worded email to the DPO at the email address earlier provided.</p>
					<p><strong>6. Protection of Personal Data</strong></p>
					<p>Yoga Pod uses commercially reasonable physical, managerial, and technical safeguards to preserve the integrity and security of your Personal Data and will not knowingly allow access to this data to anyone outside Yoga Pod, other than to you or as described in this Policy. However, Yoga Pod cannot ensure or warrant the security of any information you transmit to Yoga Pod and you do so entirely at your own risk. In particular, Yoga Pod does not warrant that such information may not be accessed, altered, collected, copied, destroyed, disposed of disclosed or modified by breach of any of Yoga Pod's physical, technical, or managerial safeguards.<br /><br /><strong>7. Access and Correction of Personal Data</strong></p>
					<p>7.1 In accordance with Clause 2.1 of this Policy, you have the right to:</p>
					<p>a) check whether Yoga Pod holds any Personal Data relating to you and, if so, obtain copies of such data; and<br />b) require Yoga Pod to correct any Personal Data relating to you which is inaccurate for the purpose for which it is being used.</p>
					<p>7.2 Yoga Pod reserves the right to charge a reasonable administrative fee in order to meet your requests under Clause 7.1.  Upon paYoga Podent of the requisite fee, your request shall be processed within a reasonable time.</p>
					<p>7.3 If you wish to verify the details you have submitted to Yoga Pod or if you wish to check on the manner in which Yoga Pod uses and processes your Personal Data, Yoga Pod's security procedures mean that Yoga Pod may request proof of identity before we reveal information. This proof of identity will take the form of full details of name, membership number and NRIC or Passport or FIN number.  You must therefore keep this information safe as you will be responsible for any action which Yoga Pod takes in response to a request from someone using your membership details. We would strongly recommend that you do not use the browser's password memory function as that would permit other people using your terminal to access your personal information.</p>
					<p><strong>8. Storage and Retention of Personal Data</strong></p>
					<p>Yoga Pod will delete, as reasonably possible, or otherwise anonYoga Podise any Personal Data in the event that the Personal Data is not required for any reasonable business or legal purposes of Yoga Pod and where the Personal Data is deleted from Yoga Pod's electronic, manual, and other filing systems in accordance with Yoga Pod's internal procedures and/or other agreements.</p>
					<p><strong>9. Change Policy</strong><br /></p>
					<p>Yoga Pod reserves the right to alter any of the clauses contained herein in compliance with local legislation and/or to meet its global policy requirements, and for any other purpose deemed reasonably necessary by Yoga Pod. You should look at these terms regularly. If you do not agree to the modified terms, you should inform us as soon as possible of the terms to which you do not consent.  Pending such notice, if there is any inconsistency between these terms and the additional terms, the additional terms will prevail to the extent of the inconsistency.</p>
					<p><strong>10. Governing Law</strong></p>
					<p>This Policy is governed by and shall be construed in accordance with the laws of Vietnam. You hereby submit to the non-exclusive jurisdiction of the Vietnam courts.</p>
					<p><strong>11. Miscellaneous</strong></p>
					<p>11.1 This Policy only applies to the collection and use of Personal Data by Yoga Pod. It does not cover third party sites to which we provide links, even if such sites are co-branded with our logo. Yoga Pod does not share your Personal Data with third party websites. Yoga Pod is not responsible for the privacy and conduct practices of these third party websites, so you should read their own privacy policies before disclosure of any Personal Data to these websites.</p>
					<p>11.2 Yoga Pod will not sell your personal information to any third party without your permission, but we cannot be responsible or held liable for the actions of third party sites which you may have linked or been directed to Yoga Pod's website.</p>
					<p>11.3 Yoga Pod's websites do not target and are not intended to attract children under the age of 18 years old. Yoga Pod does not knowingly solicit personal information from children under the age of 18 years old or send them requests for personal data.</p>
                </dl>
            </div>
        </section>
    </main>
<?php include 'Footer.php';?>

</div>

<script src="js/script.js"></script>
<!--coded by united-->
</body>
</html>