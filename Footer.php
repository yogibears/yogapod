    <!--========================================================
                              FOOTER
    =========================================================-->
    <footer>
        <div class="container">
            Yoga Pod © <span id="copyright-year"></span> All Rights Reserved &nbsp;|&nbsp;
            <a href="privacy.php">Privacy Policy</a><BR>
			<a href='http://www.yogafinder.com/' target='_blank'><img src='http://www.yogafinder.com/yogaimages/yflogo_150X140.Gif' alt="YogaFinder.com" border=0 ></a>
        </div>
    </footer>