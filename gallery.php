<?php
$home = '';
$about = '';
$classes = '';
$news = '';
$vr = '';
$contact = '';
$gallery = 'class="active"';
?>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Yoga Pod - Gallery</title>

<script type='text/javascript' src='unitegallery/js/jquery-11.0.min.js'></script>	

<script type='text/javascript' src='unitegallery/js/ug-common-libraries.js'></script>	
<script type='text/javascript' src='unitegallery/js/ug-functions.js'></script>
<script type='text/javascript' src='unitegallery/js/ug-thumbsgeneral.js'></script>
<script type='text/javascript' src='unitegallery/js/ug-thumbsstrip.js'></script>
<script type='text/javascript' src='unitegallery/js/ug-touchthumbs.js'></script>
<script type='text/javascript' src='unitegallery/js/ug-panelsbase.js'></script>
<script type='text/javascript' src='unitegallery/js/ug-strippanel.js'></script>
<script type='text/javascript' src='unitegallery/js/ug-gridpanel.js'></script>
<script type='text/javascript' src='unitegallery/js/ug-thumbsgrid.js'></script>
<script type='text/javascript' src='unitegallery/js/ug-tiles.js'></script>
<script type='text/javascript' src='unitegallery/js/ug-tiledesign.js'></script>
<script type='text/javascript' src='unitegallery/js/ug-avia.js'></script>
<script type='text/javascript' src='unitegallery/js/ug-slider.js'></script>
<script type='text/javascript' src='unitegallery/js/ug-sliderassets.js'></script>
<script type='text/javascript' src='unitegallery/js/ug-touchslider.js'></script>
<script type='text/javascript' src='unitegallery/js/ug-zoomslider.js'></script>	
<script type='text/javascript' src='unitegallery/js/ug-video.js'></script>
<script type='text/javascript' src='unitegallery/js/ug-gallery.js'></script>
<script type='text/javascript' src='unitegallery/js/ug-lightbox.js'></script>
<script type='text/javascript' src='unitegallery/js/ug-carousel.js'></script>
<script type='text/javascript' src='unitegallery/js/ug-api.js'></script>

<link rel='stylesheet' href='unitegallery/css/unite-gallery.css' type='text/css' /> 

<script type='text/javascript' src='unitegallery/themes/default/ug-theme-default.js'></script> 
<link rel='stylesheet' href='unitegallery/themes/default/ug-theme-default.css' type='text/css' /> 	
<link rel='stylesheet' 	href='unitegallery/skins/alexis/alexis.css' type='text/css' />	


<?php
$lang = $_GET['lang'];
if ($lang == "" or $lang == "english") {
    $lang = "english";
} else {
    $lang = "vietnam";
}
?>

    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no"/>
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/camera.css"/>
    <link rel="stylesheet" href="css/owl-carousel.css"/>
	


<script src='js/device.min.js'></script>

<?php include 'Header.php';?>


    <!--========================================================
                              CONTENT
    =========================================================-->
    <main>
        <section class="well well__ins1">
		<h4 class="secondary-color center">Yoga Poses</h4>
            <div class="container center">
                <h6>&nbsp;</h6>
				<div id="gallery">
					<img src="gallery/IMG_1010.jpg" data-image="gallery/IMG_1010.jpg" data-description="">
					<img src="gallery/IMG_1014.jpg" data-image="gallery/IMG_1014.jpg" data-description="">
					<img src="gallery/IMG_1016.jpg" data-image="gallery/IMG_1016.jpg" data-description="">
					<img src="gallery/IMG_1022.jpg" data-image="gallery/IMG_1022.jpg" data-description="">
					<img src="gallery/IMG_1025.jpg" data-image="gallery/IMG_1025.jpg" data-description="">
					<img src="gallery/IMG_1030.jpg" data-image="gallery/IMG_1030.jpg" data-description="">
					<img src="gallery/IMG_1041.jpg" data-image="gallery/IMG_1041.jpg" data-description="">
					<img src="gallery/IMG_1045.jpg" data-image="gallery/IMG_1045.jpg" data-description="">
					<img src="gallery/IMG_1049.jpg" data-image="gallery/IMG_1049.jpg" data-description="">
					<img src="gallery/IMG_1051.jpg" data-image="gallery/IMG_1051.jpg" data-description="">
					<img src="gallery/IMG_1056.jpg" data-image="gallery/IMG_1056.jpg" data-description="">
					<img src="gallery/IMG_1061.jpg" data-image="gallery/IMG_1061.jpg" data-description="">
					<img src="gallery/IMG_0929.jpg" data-image="gallery/IMG_0929.jpg" data-description="">
					<img src="gallery/IMG_0993.jpg" data-image="gallery/IMG_0993.jpg" data-description="">
					<img src="gallery/IMG_1002.jpg" data-image="gallery/IMG_1002.jpg" data-description="">
					<img src="gallery/IMG_1005.jpg" data-image="gallery/IMG_1005.jpg" data-description="">					
					<img src="gallery/IMG_1064.jpg" data-image="gallery/IMG_1064.jpg" data-description="">
					<img src="gallery/IMG_1066.jpg" data-image="gallery/IMG_1066.jpg" data-description="">
					<img src="gallery/IMG_1067.jpg" data-image="gallery/IMG_1067.jpg" data-description="">
					<img src="gallery/IMG_1072.jpg" data-image="gallery/IMG_1072.jpg" data-description="">
					<img src="gallery/TTT_2696.jpg" data-image="gallery/TTT_2696.jpg" data-description="">
					<img src="gallery/TTT_2828.jpg" data-image="gallery/TTT_2828.jpg" data-description="">
					<img src="gallery/TTT_2833.jpg" data-image="gallery/TTT_2833.jpg" data-description="">
					<img src="gallery/TTT_2839.jpg" data-image="gallery/TTT_2839.jpg" data-description="">
					<img src="gallery/TTT_2846.jpg" data-image="gallery/TTT_2846.jpg" data-description="">
					<img src="gallery/TTT_2850.jpg" data-image="gallery/TTT_2850.jpg" data-description="">
					<img src="gallery/TTT_2855.jpg" data-image="gallery/TTT_2855.jpg" data-description="">
					<img src="gallery/TTT_2868.jpg" data-image="gallery/TTT_2868.jpg" data-description="">
					<img src="gallery/TTT_2870.jpg" data-image="gallery/TTT_2870.jpg" data-description="">
					<img src="gallery/TTT_2874.jpg" data-image="gallery/TTT_2874.jpg" data-description="">
					<img src="gallery/IMG_0890.jpg" data-image="gallery/IMG_0890.jpg" data-description="">
					<img src="gallery/IMG_0912.jpg" data-image="gallery/IMG_0912.jpg" data-description="">
				</div>				
            </div>
        </section>
        <section class="well well__ins1">
		<h4 class="secondary-color center">Soft Opening</h4>
            <div class="container center">
                <h6>&nbsp;</h6>
				<div id="gallery1">
					<img src="gallery/TTT_2804.jpg" data-image="gallery/TTT_2804.jpg" data-description="">				
					<img src="gallery/imageofPOD.jpg" data-image="gallery/imageofPOD.jpg" data-description="">
					<img src="gallery/IMG_0518.jpg" data-image="gallery/IMG_0518.jpg" data-description="">
					<img src="gallery/IMG_0546.jpg" data-image="gallery/IMG_0546.jpg" data-description="">
					<img src="gallery/IMG_0550.jpg" data-image="gallery/IMG_0550.jpg" data-description="">
					<img src="gallery/IMG_0595.jpg" data-image="gallery/IMG_0595.jpg" data-description="">
					<img src="gallery/IMG_0723.jpg" data-image="gallery/IMG_0723.jpg" data-description="">
					<img src="gallery/IMG_0740.jpg" data-image="gallery/IMG_0740.jpg" data-description="">
					<img src="gallery/IMG_0742.jpg" data-image="gallery/IMG_0742.jpg" data-description="">
					<img src="gallery/IMG_0746.jpg" data-image="gallery/IMG_0746.jpg" data-description="">
					<img src="gallery/IMG_0770.jpg" data-image="gallery/IMG_0770.jpg" data-description="">
					<img src="gallery/IMG_0943.jpg" data-image="gallery/IMG_0943.jpg" data-description="">
					<img src="gallery/IMG_3420.jpg" data-image="gallery/IMG_3420.jpg" data-description="">
					<img src="gallery/IMG_3456.jpg" data-image="gallery/IMG_3456.jpg" data-description="">
					<img src="gallery/IMG_3485.jpg" data-image="gallery/IMG_3485.jpg" data-description="">
					<img src="gallery/IMG_3570.jpg" data-image="gallery/IMG_3570.jpg" data-description="">
					<img src="gallery/TTT_2412.jpg" data-image="gallery/TTT_2412.jpg" data-description="">
					<img src="gallery/TTT_2416.jpg" data-image="gallery/TTT_2416.jpg" data-description="">
					<img src="gallery/TTT_2421.jpg" data-image="gallery/TTT_2421.jpg" data-description="">
					<img src="gallery/TTT_2422.jpg" data-image="gallery/TTT_2422.jpg" data-description="">
					<img src="gallery/TTT_2426.jpg" data-image="gallery/TTT_2426.jpg" data-description="">
					<img src="gallery/TTT_2488.jpg" data-image="gallery/TTT_2488.jpg" data-description="">
					<img src="gallery/TTT_2489.jpg" data-image="gallery/TTT_2489.jpg" data-description="">
					<img src="gallery/TTT_2502.jpg" data-image="gallery/TTT_2502.jpg" data-description="">
					<img src="gallery/TTT_2511.jpg" data-image="gallery/TTT_2511.jpg" data-description="">
					<img src="gallery/TTT_2538.jpg" data-image="gallery/TTT_2538.jpg" data-description="">
					<img src="gallery/TTT_2539.jpg" data-image="gallery/TTT_2539.jpg" data-description="">
					<img src="gallery/TTT_2542.jpg" data-image="gallery/TTT_2542.jpg" data-description="">
					<img src="gallery/TTT_2553.jpg" data-image="gallery/TTT_2553.jpg" data-description="">
					<img src="gallery/TTT_2561.jpg" data-image="gallery/TTT_2561.jpg" data-description="">
					<img src="gallery/TTT_2579.jpg" data-image="gallery/TTT_2579.jpg" data-description="">
					<img src="gallery/TTT_2609.jpg" data-image="gallery/TTT_2609.jpg" data-description="">
					<img src="gallery/TTT_2647.jpg" data-image="gallery/TTT_2647.jpg" data-description="">
					<img src="gallery/TTT_2670.jpg" data-image="gallery/TTT_2670.jpg" data-description="">
					<img src="gallery/TTT_2750.jpg" data-image="gallery/TTT_2750.jpg" data-description="">
					<img src="gallery/TTT_2770.jpg" data-image="gallery/TTT_2770.jpg" data-description="">
					<img src="gallery/TTT_2813.jpg" data-image="gallery/TTT_2813.jpg" data-description="">
					<img src="gallery/TTT_2877.jpg" data-image="gallery/TTT_2877.jpg" data-description="">
					<img src="gallery/TTT_2880.jpg" data-image="gallery/TTT_2880.jpg" data-description="">
					<img src="gallery/TTT_2886.jpg" data-image="gallery/TTT_2886.jpg" data-description="">
				</div>				
            </div>
        </section>
    </main>
<?php include 'Footer.php';?>
</div>

<script src="js/script.js"></script>
<script type="text/javascript"> 
	
	jQuery(document).ready(function(){ 
		jQuery("#gallery").unitegallery(); 
		gallery_skin:"alexis";
	}); 
	
		jQuery(document).ready(function(){ 
		jQuery("#gallery1").unitegallery(); 
	});

</script>
</body>
</html>