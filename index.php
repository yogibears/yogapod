<?php
$home = 'class="active"';
?>
<!doctype html>
<html lang="en">
<head>
    <title>Yoga Pod - Home</title>

<?php
$lang = $_GET['lang'];
if ($lang == "" or $lang == "english") {
    $lang = "english";
} else {
    $lang = "vietnam";
}
?>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no"/>
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/camera.css"/>
    <link rel="stylesheet" href="css/owl-carousel.css"/>
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script>

    <!--[if lt IE 9]>
    <html class="lt-ie9">
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/..">
            <img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"
                 alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
        </a>
    </div>
    <script src="js/html5shiv.js"></script>
    <![endif]-->

    <script src='js/device.min.js'></script>
	
<?php include 'Header.php';?>

	
    <!--========================================================
                              CONTENT
    =========================================================-->
    <main>
        <section>
            <div class="camera_container">
                <div id="camera" class="camera_wrap">
                    <div data-src="images/page-1_slide01.jpg">
                        <div class="camera_caption fadeIn">
                            <div class="container center">
                                <div class="row">
                                    <div class="preffix_2 grid_8">
                                        <h4 class="english">Breathe new life into your practices</h4>
                                        <h4 class="vietnam">Làn gió mới cho những giờ luyện tập</h3>
                                        <!-- <a class="btn" href="#">read more</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-src="images/page-1_slide02.jpg">
                        <div class="camera_caption fadeIn">
                            <div class="container center">
                                <div class="row">
                                    <div class="preffix_2 grid_8">
                                        <h4 class="english">Relaxing moments at the heart of Thao Dien district 2</h3>
                                        <h4 class="vietnam">Khoảnh khắc thư giãn ngay giữa lòng Thảo Điền Quận 2</h3>
                                        <!-- <a class="btn" href="#">read more</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-src="images/page-1_slide03.jpg">
                        <div class="camera_caption fadeIn">
                            <div class="container center">
                                <div class="row">
                                    <div class="preffix_2 grid_8">
                                        <h4 class="english">Fresh air from nature for souls who loves Yoga</h3>
                                        <h4 class="vietnam">Hơi thở mát lành từ thiên nhiên cho những tâm hồn yêu Yoga</h3>										
                                        <!-- <a class="btn" href="#">read more</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="well">
            <div class="container">
                <h4 class="english center secondary-color">Special benefits of Yoga Pod</h4>
                <h4 class="vietnam center secondary-color">Những tiện ích đặc biệt của Yoga Pod</h4>
                <h6 class="english center">A green and open space where your respiration is taken care of</h6>
                <h6 class="vietnam center">AKhông gian xanh mát và trong lành trọn vẹn cho từng hơi thở an nhiên</h6>

                <ul class="row index-list">
                    <li class="grid_4 wow fadeInRight" data-equal-group="1" >
                        <div class="wrapp">
                            <h6 class="english">A peaceful secret garden</h6>
                            <h6 class="vietnam">Khu vườn bí mật yên tĩnh<BR><BR></h6>
                            <p class="english">First time ever in Saigon, a space to practice Yoga is purposefully designed within a verdant, pure and silent garden </p>
                            <p class="vietnam">Lần đầu tiên tại Sài Gòn, không gian luyện tập yoga được thiết kế theo hướng khu vườn xanh mát, trong lành và an yên </p>
                        </div>
                    </li>

                    <li class="grid_4 wow fadeInRight" data-equal-group="1" data-wow-delay="0.2s">
                        <div class="wrapp">
                          <h6 class="english">Everyone in your family can enjoy yoga</h6>
                          <h6 class="vietnam">Mọi thành viên trong gia đình đều có thể tận hưởng yoga</h6>
                          <p class="english">You can easily book a mat for your kids; special session for pregnancy and private classes at home with professional trainers</p>
                          <p class="vietnam">Bạn hoàn toàn dễ dàng đăng ký lớp học dành riêng cho trẻ em, mẹ bầu và lớp riêng tại nhà với các huấn luyện viên chuyên nghiệp</p>
                        </div>
                    </li>
                    <li class="grid_4 wow fadeInRight" data-equal-group="1" data-wow-delay="0.4s">
                        <div class="wrapp">
                            <h6 class="english">Newbies are highly welcomed</h6>
                            <h6 class="vietnam">Người mới bắt đầu làm quen yoga luôn được chào đón</h6>
                            <p class="english">Don’t be afraid of trying out yoga as you can always find your customized classes at the first attempt and our packages are all short-term</p>
                            <p class="vietnam">Vì bạn có thể dễ dàng tìm được các lớp yoga phù hợp dành riêng cho lần thử sức đầu tiên cũng như qua các gói tập ngắn hạn của Yoga Pod
</p>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
    </main>
<?php include 'Footer.php';?>
</div>

<script src="js/script.js"></script>
</body>
</html>